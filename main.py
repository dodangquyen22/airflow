import pandas as pd
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import pydoop.hdfs as hdfs
from datetime import datetime

file_paths = [
    r'/root/airflow/data/bitcoin.csv',
    r'/root/airflow/data/ethereum.csv',
    r'/root/airflow/data/polkadot.csv',
]
path = '/root/airflow/data/raw'

def process_data(file_path):
    df = pd.read_csv(file_path)
    if df['date'].str.contains(':').any():
        df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d %H:%M:%S.%f')
    else:
        df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')

    for index, row in df.iterrows():
        date = row['date'].date()
        file_name = f"{path}/{row['coin_name']}/{date}.csv"
        try:
            with open(file_name, 'a') as file:
                file.write(
                    f"{row['date']},{row['price']},{row['total_volume']},{row['market_cap']},{row['coin_name']}\n"
                )
        except Exception as e:
            print(f"Error writing to HDFS: {e}")

def included():
    for file_path in file_paths:
        process_data(file_path)

dag = DAG(
    dag_id='main',
    description='Push file to HDFS',
    schedule_interval=None,
    start_date=datetime(2024, 3, 15),
)

save_file_locally = PythonOperator(
    task_id='save_file_locally',
    python_callable=included,
    dag=dag
)

push_to_hdfs = BashOperator(
    task_id='push_to_hdfs',
    bash_command='hdfs dfs -put /root/airflow/data/raw/* /data/raw',
    dag=dag
)

save_file_locally >> push_to_hdfs

